package com.homeworks.weekone;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {

	@GetMapping("/student")
	public String student() {
		return "Welcome Sokhket";
	}
}
